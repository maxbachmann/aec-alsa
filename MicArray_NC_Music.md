## `/etc/asound.conf` ermöglicht Noise Cancelling beim Respeaker Mic Array 2.0

### Datei für Copy&Paste

Wichtig: Die Soundkarten-Nummern müssen angepasst werden! Sie können mit `aplay -l` herausgefunden werden.

```
pcm.2devices_mixer {
  type dmix
  ipc_key 1024
  ipc_perm 0666
  slave.pcm "hw:<card of Respeaker>,0"  # Respeaker Playback
  slave {
    period_size 2000
    buffer_size 8000
    rate 44100
    format S24_3LE
    channels 2
  }
  bindings {
    0 0
    1 1
  }
}

pcm.music_4channels {
    type multi
    slaves.a.pcm "hw:<card of USB sound>,0"
    slaves.a.channels 2
    slaves.b.pcm "plug:2devices_mixer"
    slaves.b.channels 2
    bindings.0 { slave a; channel 0; }
    bindings.1 { slave a; channel 1; }
    bindings.2 { slave b; channel 0; }
    bindings.3 { slave b; channel 1; }
}

pcm.tts_4channels {
    type multi
    slaves.a.pcm "hw:<card of internal sound>,0"
    slaves.a.channels 2
    slaves.b.pcm "plug:2devices_mixer"
    slaves.b.channels 2
    bindings.0 { slave a; channel 0; }
    bindings.1 { slave a; channel 1; }
    bindings.2 { slave b; channel 0; }
    bindings.3 { slave b; channel 1; }
}

pcm.music_playback {
    type route
    slave.pcm "music_4channels"
    slave.channels 4
    ttable.0.0 1
    ttable.1.1 1
    ttable.0.2 1
    ttable.1.3 1
}

pcm.tts_playback {
    type route
    slave.pcm "tts_4channels"
    slave.channels 4
    ttable.0.0 1
    ttable.1.1 1
    ttable.0.2 1
    ttable.1.3 1
}

pcm.!default {
  type asym
  playback.pcm "tts_playback"  # Snips Soundserver will use this
  capture.pcm {
    type plug
    slave.pcm "hw:<card of respeaker>,0"  # Respeaker Capture
  }
}
```

### Schritte für das Testen mit Musik

1. Zwei Terminal öffnen, die mit Raspberry Pi über SSH verbunden sind (oder direkt auf dem Raspberry Pi)
2. Terminal #1: `sudo systemctl stop snips-audio-server`
3. Musik auf den Raspberry Pi laden
4. Terminal #2: `mpg123 -a music_playback ~/<path of music>/*`
5. Terminal #1: `arecord -D plughw:<card of Respeaker>,0 -f cd test.wav `
   Hinweis: Die Soundkarten-Nummer vom Respeaker kann man mit `aplay -l` herausfinden
6. Terminal #1: Strg+C um Aufnahme zu beenden
7. Terminal #2: Strg+C um Musik zu stoppen
8. Terminal #1: `aplay -D plug:music_playback test.wav `

### Bluetooth-Unterstützung

1. `sudo apt update && sudo apt install -y bluealsa`

2. `sudo usermod -a -G bluetooth pi`

3. `sudo reboot`

4. `bluetoothctl`

   1. `scan on`
   2. Gerät wird angezeigt
   3. `pair xx:xx` (statt xx:xx die Kennung vom Gerät)
   4. `trust xx:xx`
   5. `connect xx:xx`
   6. `exit`

   ```
   pcm.bluetooth_speaker {
     type plug
     slave.pcm {
       type bluealsa
       device "04:FE:A1:CF:AE:49"
       profile "a2dp"
       interface "hci0"
     }
     hint {
       show on
       description "Bluetooth Audio ALSA Backend"
     }
   }
   ctl.bluetooth_speaker {
     type bluetooth
   }
   
   pcm.2devices_mixer {
     type dmix
     ipc_key 1024
     ipc_perm 0666  # important if multiple users!
     slave.pcm "hw:2,0"  # Respeaker Mic Array Playback
     slave {
       #period_time 0
       period_size 2000
       buffer_size 8000
       rate 44100
       format S24_3LE
       channels 2
     }
     bindings {
       0 0
       1 1
     }
   }
   
   pcm.music_4channels {
       type multi
       slaves.a.pcm "bluetooth_speaker"
       slaves.a.channels 2
       slaves.b.pcm "plug:2devices_mixer"
       slaves.b.channels 2
       bindings.0 { slave a; channel 0; }
       bindings.1 { slave a; channel 1; }
       bindings.2 { slave b; channel 0; }
       bindings.3 { slave b; channel 1; }
   }
   
   pcm.tts_4channels {
       type multi
       slaves.a.pcm "hw:0,0"
       slaves.a.channels 2
       slaves.b.pcm "plug:2devices_mixer"
       slaves.b.channels 2
       bindings.0 { slave a; channel 0; }
       bindings.1 { slave a; channel 1; }
       bindings.2 { slave b; channel 0; }
       bindings.3 { slave b; channel 1; }
   }
   
   pcm.music_playback {
       type route
       slave.pcm "music_4channels"
       slave.channels 4
       ttable.0.0 1
       ttable.1.1 1
       ttable.0.2 1
       ttable.1.3 1
   }
   
   pcm.tts_playback {
       type route
       slave.pcm "tts_4channels"
       slave.channels 4
       ttable.0.0 1
       ttable.1.1 1
       ttable.0.2 1
       ttable.1.3 1
   }
   
   pcm.!default {
     type asym
     playback.pcm "tts_playback"  # Snips Soundserver will use this
     capture.pcm {
       type plug
       slave.pcm "hw:2,0"  # Respeaker Mic Array Capture
     }
   }
   ```

   
